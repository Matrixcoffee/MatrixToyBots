# stdlib
import argparse
import re
import time

# external deps
from matrix_client_core.ratelimit import RateLimit
import matrix_client_core as botcore
import matrix_client_core.nocurses as nocurses
import matrix_client_core.notifier as notifier


class NotificationPrinter(notifier.NotificationListener):
	@staticmethod
	def on_mcc_ratelimit_ok(service, event, data):
		s_ok = ("Not OK:", "OK:")[data[0]]
		print(s_ok, data[1], data[2])

	@staticmethod
	def on_mcc_ratelimit_consume(service, event, data):
		print("Consume!", data)

	@staticmethod
	def on_mcc_ratelimit_replenish(service, event, data):
		print("Replenish!", data)

	@staticmethod
	def on_toys_WCBot_send_notice(service, event, data):
		nocurses.print("Sending message to {}: {}".format(*data), fg='red')

	@staticmethod
	def on_toys_WCBot_send_message_event(service, event, data):
		nocurses.print("Sending message to {}: {}".format(*data), fg='red')

def format_time_interval(seconds, fmt_seconds="{:.3f} seconds"):
	units = (
			(fmt_seconds, 60),
			("{} minutes and ", 60),
			("{} hours, ", 24),
			("{} days, ", 365),
			("{} years, ", None),
	)

	result = ""
	remainder = abs(seconds)
	for fmt, div in units:
		if div: remainder, unit = divmod(remainder, div)
		else: remainder, unit = 0, remainder
		remainder = int(remainder)
		result = fmt.format(unit) + result
		if remainder == 0: break

	return ("", "-")[seconds < 0] + result

class WCBot(botcore.MXClient):
	RE_WC = re.compile("\\b[wW][cC]\\b(?! \U0001f6bd)")

	def _wc(self, text):
		if text is None: return None

		result = []
		prev_pos = 0

		for m in self.RE_WC.finditer(text):
			pos = m.end()
			result.append(text[prev_pos:pos])
			result.append(" \U0001f6bd")
			prev_pos = m.end()

		if len(result) == 0: return None

		result.append(text[prev_pos:])
		return "".join(result)

	@botcore.wrap_exception
	def on_m_room_message(self, event):
		now = time.time()
		self.last_event = event
		if not event['content']['msgtype'] == "m.text": return

		sender = event['sender']
		eventid = event['event_id']
		roomid = event['room_id']
		inmsg = event['content']['body']
		try: fmt_inmsg = event['content']['formatted_body']
		except KeyError: fmt_inmsg = None

		# flood protection
		if not self.ratelimit.ok(): return

		# Do the business
		reply = self._wc(inmsg)
		fmt_reply = self._wc(fmt_inmsg)
		if not reply and not fmt_reply: return

		# Construct reply event
		if reply is None: reply = inmsg
		body = {
			"msgtype": "m.notice",
			"body": reply
		}
		if fmt_reply is not None:
			body['formatted_body'] = fmt_reply
			body['format'] = 'org.matrix.custom.html'

		# Send
		self.ratelimit.consume()
		notifier.notify(__file__, 'toys.WCBot.send_message_event', (roomid, repr(body)))
		self.sdkclient.api.send_message_event(roomid, "m.room.message", body)

	def connect(self):
		self.ratelimit = RateLimit(0.8, 2.0)
		self.ratelimit.start_replenish_thread(60)

		self.sync_filter = '''{
			"presence": { "types": [ "" ] },
			"room": {
				"ephemeral": { "types": [ "" ] },
				"state": {
					"types": [
						"m.room.canonical_alias",
						"m.room.aliases",
						"m.room.name",
						"m.room.topic"
					]
				},
				"timeline": {
					"types": [ "*" ],
					"limit": 3
				}
			}
		}'''

		self.is_bot = True
		self.login()
		self.first_sync()
		self.sdkclient.add_listener(self.on_m_room_message, 'm.room.message')
		#self.start_send_thread(self.sdkclient.api.send_notice)
		self.hook()

	def run_forever(self):
		print("Ready.")
		self.repl()


def parse_args():
	parser = argparse.ArgumentParser(description="PongBot responds to !ping and calculates the time difference based on the sender's timestamp.")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debug messages.")
	options = parser.parse_args()
	return options

if __name__ == '__main__':
	try: import site_config
	except ImportError: pass

	opts = parse_args()
	if opts.debug: np = NotificationPrinter()

	bot = WCBot('account-wcbot.json')
	bot.connect()
	bot.run_forever()
