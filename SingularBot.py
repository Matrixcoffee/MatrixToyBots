# stdlib
import argparse
import collections
import dbm
import random
try: random = random.SystemRandom()
except: pass
import re
import time

# external deps
from matrix_client_core.ratelimit import RateLimit
import matrix_client_core as botcore
import matrix_client_core.nocurses as nocurses
import matrix_client_core.notifier as notifier

minutes = 60

def trace(*args):
	notifier.notify(__file__, 'toys.SingularBot.trace', args)

class NotificationPrinter(notifier.NotificationListener):
	@staticmethod
	def on_mcc_ratelimit_ok(service, event, data):
		s_ok = ("Not OK:", "OK:")[data[0]]
		print(s_ok, data[1], data[2])

	@staticmethod
	def on_mcc_ratelimit_consume(service, event, data):
		print("Consume!", data)

	@staticmethod
	def on_mcc_ratelimit_replenish(service, event, data):
		print("Replenish!", data)

	@staticmethod
	def on_toys_SingularBot_trace(service, event, data):
		print(event + ":", *data)

	@staticmethod
	def on_toys_SingularBot_send_notice(service, event, data):
		nocurses.print("Sending message to {}: {}".format(*data), fg='red')


class RecentDB:
	def __init__(self, expire_seconds=120, normalize=(lambda x: x.lower())):
		self.expire_seconds = expire_seconds
		if normalize is None: normalize = (lambda x: x)
		self.normalize = normalize
		self.recent = collections.deque()

	def add(self, word):
		self.recent.append(int(time.time()))
		trace("add:", repr(self.recent[-1]))
		self.recent.append(self.normalize(word))
		trace("add:", repr(self.recent[-1]))

	def __contains__(self, item):
		return self.normalize(item) in self.recent

	def run_maintenance(self):
		try:	i = self.recent[0]
		except:	return False

		if isinstance(i, int) and i + self.expire_seconds > time.time():
			return False

		p = self.recent.popleft()
		trace("pop:", repr(p))
		return True


class Noun:
	def __init__(self, singular, plural):
		self.singular = singular
		self.plural = plural

	def __str__(self):
		return self.plural

	def __repr__(self):
		return "{}({!r}, {!r})".format(self.__class__.__name__, self.singular, self.plural)


class WordDB:
	def __init__(self, filename):
		self.filename = filename
		self.db = dbm.open(filename, 'c')

	def __getitem__(self, word):
		try:
			return int(self.db[str(word).lower().encode('utf-8')].decode('ascii'))
		except KeyError:
			return 0

	def __setitem__(self, word, value):
		self.db[str(word).lower().encode('utf-8')] = str(value).encode('ascii')


class Singularizer:
	RE_CLEANWORD = re.compile("""^["']?([A-Za-z][a-z]*[ai])["'?!.,]*$""")
	RE_RUN = re.compile("""([a-z])\\1\\1""", re.IGNORECASE) # Runs of 3+ identical letters

	def __init__(self, dbfile):
		self.db = WordDB(dbfile)
		self.recent = RecentDB()

	def singularize(self, text):
		trace("singularize:", repr(text))
		result = []
		words = text.split()
		trace("words:", repr(words))
		for word in words:
			m = self.RE_CLEANWORD.search(word)
			if not m: continue

			word = m.group(1)
			if self.RE_RUN.search(word):
				trace("{!r}: 3+".format(word))
				continue # Disregard words with runs of 3+ identical letters

			if word.endswith("i"):
				result.append(Noun(word[:-1] + "us", word))
				trace("New noun:", repr(result[-1]))
			elif word.endswith("a"):
				result.append(Noun(word[:-1] + "um", word))
				trace("New noun:", repr(result[-1]))

		return result

	@staticmethod
	def make_reply(noun):
		return '"{0}?" Actually, the singular is {1}.' \
			.format(noun.plural.capitalize(), noun.singular.lower())

	def handle_network(self, text):
		if not hasattr(self, 'ratelimit'):
			self.ratelimit = RateLimit(0.5, 1.0)
			self.ratelimit.start_replenish_thread(30*minutes)

		self.recent.run_maintenance()

		if not self.ratelimit.ok(): return None

		singulars = self.singularize(text)

		while singulars:
			i = random.randrange(len(singulars))
			s = singulars.pop(i)
			trace("Considering {}, {} remaining".format(s, len(singulars)))

			if s.plural in self.recent:
				trace("Rejecting", s, "due to recent")
				continue

			count = self.db[s]
			probability = 0.9 ** count
			r = random.random()
			ok = r <= probability
			trace("Word: {} Count: {} Probability: {} Roll: {}, Outcome: {!r}" \
				.format(s, count, probability, r, ok))
			if not ok: continue

			self.ratelimit.consume()
			self.db[s] = count + 1
			self.recent.add(s.plural)
			return self.make_reply(s)

		return None

class SingularBot(botcore.MXClient):
	@botcore.wrap_exception
	def on_m_room_message(self, event):
		self.last_event = event
		if not event['content']['msgtype'] == "m.text": return

		sender = event['sender']
		eventid = event['event_id']
		roomid = event['room_id']
		inmsg = event['content']['body']

		reply = self.singularizer.handle_network(inmsg)
		if reply:
			notifier.notify(__file__, 'toys.SingularBot.send_notice', (roomid, reply))
			self.sendmsg(roomid, reply)

	def set_database(self, filename):
		self.dbfilename = filename
		self.singularizer = Singularizer(filename)
		return self

	def connect(self):
		self.sync_filter = '''{
			"presence": { "types": [ "" ] },
			"room": {
				"ephemeral": { "types": [ "" ] },
				"state": {
					"types": [
						"m.room.canonical_alias",
						"m.room.aliases",
						"m.room.name",
						"m.room.topic"
					]
				},
				"timeline": {
					"types": [ "*" ],
					"limit": 3
				}
			}
		}'''

		self.is_bot = True
		self.login()
		self.first_sync()
		self.sdkclient.add_listener(self.on_m_room_message, 'm.room.message')
		self.start_send_thread(self.sdkclient.api.send_notice)
		self.hook()

	def run_forever(self):
		print("Ready.")
		self.repl()


def parse_args():
	parser = argparse.ArgumentParser(description="SingularBot detects words ending in -i or -a, and changes them into the latin singular. Regardless of whether the original word was latin or not.")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debug messages.")
	parser.add_argument("-t", "--test", action='store_true', default=False, help="Local testmode. Replies to stdin instead of connecting to the network.")
	parser.add_argument("-T", "--test-full", action='store_true', default=False, help="Local testmode. Handle stdin as if messages came from a single room, including rate limiting and everything.")
	parser.add_argument("--database", default="database-singularbot", help="Filename of the database, without extension.")
	options = parser.parse_args()
	return options

if __name__ == '__main__':
	try: import site_config
	except ImportError: pass

	opts = parse_args()
	if opts.debug: np = NotificationPrinter()
	if opts.test:
		sr = Singularizer(opts.database)
		print("Ready.")
		while True:
			text = input()
			if text == "": break
			singulars = sr.singularize(text)
			for s in singulars:
				print(sr.make_reply(s))
	elif opts.test_full:
		sr = Singularizer(opts.database)
		print("Ready.")
		while True:
			text = input()
			if text == "": break
			reply = sr.handle_network(text)
			if reply: print(reply)
	else:
		bot = SingularBot('account-singularbot.json').set_database(opts.database)
		bot.connect()
		bot.run_forever()
