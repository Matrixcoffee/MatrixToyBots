# Matrix Toy Bots
[Matrix Toy Bots](https://gitlab.com/Matrixcoffee/MatrixToyBots) is a collection of tiny [Matrix](https://matrix.org) bots.

## Why does it exist?
Because sometimes you gotta have fun.

## Status
**Alpha**. These bots are provided as-is, without warranty of any kind. Shake
well before use. May contain traces of Nuts.

## Provides
* **PongBot**. Goes "Pong" when you go "!ping". Attempts to measure latency, but accuracy is limited due to it only being able to measure one leg of the round-trip.
* **SingularBot**. Converts latin plurals into latin singulars. Regardless of whether the original words were latin. Blatantly ripped off from [The Grey Literature](https://www.bgcarlisle.com/blog/2018/08/24/introducing-actually-the-singular-is-a-mastobot/).
* **SmileBot**. Cheers you up with a smile when you're sad. Smiles even bigger when you're happy.

## Recommended Installation
```
$ cd $SOME_EMPTY_DIR
$ git clone https://github.com/matrix-org/matrix-python-sdk.git
$ git clone https://gitlab.com/Matrixcoffee/matrix-client-core.git
$ git clone https://gitlab.com/Matrixcoffee/urllib-requests-adapter.git (optional if requests is already installed)
$ wget -P urllib-requests-adapter https://github.com/Anorov/PySocks/raw/master/socks.py # (if socks is not installed _and_ you need it, e.g. on TAILS)
$ git clone https://gitlab.com/Matrixcoffee/MatrixToyBots.git
```
Run any one of the shell scripts to start the associated bot.

None of the bots know how to register an account, so you will need to
register by other means. [Riot.im](https://riot.im/app), for example.

Have fun!

## License
Copyright 2018 @Coffee:matrix.org

   > Licensed under the Apache License, Version 2.0 (the "License");
   > you may not use this file except in compliance with the License.

   > The full text of the License can be obtained from the file called [LICENSE](LICENSE).

   > Unless required by applicable law or agreed to in writing, software
   > distributed under the License is distributed on an "AS IS" BASIS,
   > WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   > See the License for the specific language governing permissions and
   > limitations under the License.
