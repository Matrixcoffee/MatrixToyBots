# stdlib
import argparse
import re

# external deps
from matrix_client_core.ratelimit import RateLimit
import matrix_client_core as botcore
import matrix_client_core.notifier as notifier


class NotificationPrinter(notifier.NotificationListener):
	@staticmethod
	def on_mcc_ratelimit_ok(service, event, data):
		s_ok = ("Not OK:", "OK:")[data[0]]
		print(s_ok, data[1], data[2])

	@staticmethod
	def on_mcc_ratelimit_consume(service, event, data):
		print("Consume!", data)

	@staticmethod
	def on_mcc_ratelimit_replenish(service, event, data):
		print("Replenish!", data)


class SmileBot(botcore.MXClient):
	RE_FROWN = re.compile("^\s*([>+]?[;:=][Oo*^+-]?)([({<[]+)\s*$")
	RE_SMILE = re.compile("^\s*([>+]?[;:=][Oo*^+-]?)([])}>pPD]+)\s*$")
	SMILES = dict(map(lambda x: x.split(), (\
	 ":( :)  >:( :)  :^( :)  >:^( :)  :(( :)  :^(( :)  >:(( :)  " + \
	 ":) :))  :)) :)))  :))) :p  :)))) :p  " + \
	 ":p :pp  :pp :ppp  :ppp :D  :pppp :D  " + \
	 ":d :DD  :dd :DDD  :ddd :)  :dddd :)  " + \
	 ":< :>  :{ :}  ;( ;)  :^( :^)" \
		).split("  ")))
	INVERTS = dict("() {} [] <>".split())
	PROGRESS = ")DPP"

	@classmethod
	def _old_calculate_reply(klass, text):
		text = text.lower()
		try:
			reply = klass.SMILES[text]
		except KeyError:
			reply = ":)"
			if not klass.RE_FROWN.search(text): return None
		return reply

	@classmethod
	def calculate_reply(klass, text):
		m = klass.RE_FROWN.search(text)
		if m: return m.group(1) + klass.INVERTS[m.group(2)[0]]
		m = klass.RE_SMILE.search(text)
		if not m: return None
		l, r = m.group(1), m.group(2)
		if len(r) < 3: return l + r + r[-1]
		return l + klass.PROGRESS[klass.PROGRESS.find(r[0]) - 1]

	@botcore.wrap_exception
	def on_m_room_message(self, event):
		self.last_event = event
		if not event['content']['msgtype'] == "m.text": return

		# flood protection
		if not self.ratelimit.ok(): return

		roomid = event['room_id']
		inmsg = event['content']['body']

		reply = self.calculate_reply(inmsg)
		if reply is None: return

		self.ratelimit.consume()
		self.sendmsg(roomid, reply)

	def connect(self):
		self.ratelimit = RateLimit(0.8, 2.0)
		self.ratelimit.start_replenish_thread(60)

		self.sync_filter = '''{
			"presence": { "types": [ "" ] },
			"room": {
				"ephemeral": { "types": [ "" ] },
				"state": {
					"types": [
						"m.room.canonical_alias",
						"m.room.aliases",
						"m.room.name",
						"m.room.topic"
					]
				},
				"timeline": {
					"types": [ "*" ],
					"limit": 3
				}
			}
		}'''

		self.is_bot = True
		self.login()
		self.first_sync()
		self.sdkclient.add_listener(self.on_m_room_message, 'm.room.message')
		self.start_send_thread(self.sdkclient.api.send_notice)
		self.hook()

	def run_forever(self):
		print("Ready.")
		self.repl()


def parse_args():
	parser = argparse.ArgumentParser(description="SmileBot cheers you up when you're down.")
	parser.add_argument("-t", "--test", action='store_true', default=False, help="Enter local test mode.")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debug messages.")
	options = parser.parse_args()
	return options

if __name__ == '__main__':
	try: import site_config
	except ImportError: pass

	opts = parse_args()

	if opts.test:
		bot = SmileBot()
		while True:
			line = input().strip()
			if line == "": break
			print(bot.calculate_reply(line))
	else:
		if opts.debug: np = NotificationPrinter()

		bot = SmileBot('account-smilebot.json')
		bot.connect()
		bot.run_forever()
